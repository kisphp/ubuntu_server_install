#!/bin/bash

DIR=`pwd`

echo " " >> $DIR/readme.txt
echo " - custom/nodejs.sh - " >> $DIR/readme.txt

apt-get install -y nodejs npm
echo "installed nodejs npm " >> $DIR/readme.txt
apt-get install -y build-essential libssl-dev
echo "installed build-essential libssl-dev " >> $DIR/readme.txt

ln -s /usr/bin/nodejs /usr/bin/node
echo "created nodejs symlink " >> $DIR/readme.txt

NPM=`which npm`

echo "Upgrade NodeJS and NPM"
sudo $NPM cache clean -f
sudo $NPM install -g n
sudo n stable

$NPM install -g bower gulp
echo "installed bower gulp " >> $DIR/readme.txt

echo " --//-- " >> $DIR/readme.txt
