#!/bin/bash

DIR=`pwd`

echo " " >> $DIR/readme.txt
echo " - custom/python.sh - " >> $DIR/readme.txt

apt-get install -y python-pip
echo "installed python-pip" >> $DIR/readme.txt

pip install Sphinx
echo "installed Sphinx"

echo " --//-- " >> $DIR/readme.txt

