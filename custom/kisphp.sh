#!/bin/bash

DIR=`pwd`

echo " " >> $DIR/readme.txt
echo " - custom/kisphp.sh - " >> $DIR/readme.txt

GIT=`which git`

SUSER=`cat /etc/passwd | grep 1000 | cut -d: -f1`

mkdir /home/web/comp -p
echo "created /home/web/comp directory " >> $DIR/readme.txt

$GIT clone git@bitbucket.org:kisphp/kisphp-lib-database.git /home/web/comp/database
echo "cloned git@bitbucket.org:kisphp/kisphp-lib-database.git " >> $DIR/readme.txt
$GIT clone git@bitbucket.org:kisphp/kisphp-lib-grid.git /home/web/comp/grid
echo "cloned git@bitbucket.org:kisphp/kisphp-lib-grid.git " >> $DIR/readme.txt
$GIT clone git@bitbucket.org:kisphp/kisphp-library.git /home/web/comp/kisphp
echo "cloned git@bitbucket.org:kisphp/kisphp-library.git " >> $DIR/readme.txt
$GIT clone git@bitbucket.org:kisphp/kisphp-lib-orm.git /home/web/comp/orm
echo "cloned git@bitbucket.org:kisphp/kisphp-lib-orm.git " >> $DIR/readme.txt
$GIT clone git@bitbucket.org:kisphp/kisphp-lib-paginator.git /home/web/comp/paginator
echo "cloned git@bitbucket.org:kisphp/kisphp-lib-paginator.git " >> $DIR/readme.txt
$GIT clone git@bitbucket.org:kisphp/kisphp-lib-sdk.git /home/web/comp/sdk
echo "cloned git@bitbucket.org:kisphp/kisphp-lib-sdk.git " >> $DIR/readme.txt
$GIT clone git@bitbucket.org:kisphp/kisphp-lib-thumbnails.git /home/web/comp/thumbs
echo "cloned git@bitbucket.org:kisphp/kisphp-lib-thumbnails.git " >> $DIR/readme.txt
$GIT clone git@bitbucket.org:kisphp/kisphp-lib-xmlparser.git  /home/web/comp/xmlparser
echo "cloned git@bitbucket.org:kisphp/kisphp-lib-xmlparser.git " >> $DIR/readme.txt

cp $DIR/custom/conf_kisphp/dev.local /etc/apache2/sites-available/dev.local.conf
echo "added virtual host dev.local config file " >> $DIR/readme.txt
cp $DIR/custom/conf_kisphp/sdk.local /etc/apache2/sites-available/sdk.local.conf
echo "added virtual host sdk.local config file " >> $DIR/readme.txt

chown $SUSER: /home/web -R
echo "given write permissions to $SUSER on /home/web folder " >> $DIR/readme.txt

a2ensite dev.local
echo "enabled host dev.local " >> $DIR/readme.txt
a2ensite sdk.local
echo "enabled host sdk.local " >> $DIR/readme.txt

echo "127.0.0.1 dev.local sdk.local" >> /etc/hosts
echo "added dev.local sdk.local to /etc/hosts file " >> $DIR/readme.txt

mkdir /home/web/dev.local/public_html -p
echo "created folder /home/web/dev.local/public_html " >> $DIR/readme.txt
chown $SUSER: /home/web -R
echo "given write permissions to $SUSER on /home/web folder " >> $DIR/readme.txt

echo " "
echo " - Add MySQL database password - "
echo " "
mysql -uroot -p < $DIR/custom/conf_kisphp/database.sql

echo " " >> $DIR/readme.txt
echo "created MySQL user: 'kisphp' with password 'kisphp' " >> $DIR/readme.txt
echo " " >> $DIR/readme.txt


echo " --//-- " >> $DIR/readme.txt
