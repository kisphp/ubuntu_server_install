#!/bin/bash

DIR=`pwd`

echo " " >> $DIR/readme.txt
echo " - custom/redis.sh - " >> $DIR/readme.txt

echo " " >> $DIR/readme.txt
echo "Install redis.."
apt-get install -y redis-server

echo " "
echo "backup redis.config file"
cp /etc/redis/redis.conf /etc/redis/redis.conf.default

echo " " >> $DIR/readme.txt
echo "Write new redis.config file" >> $DIR/readme.txt

echo "daemonize yes" > /etc/redis/redis.conf
echo "pidfile /var/run/redis.pid" >> /etc/redis/redis.conf
echo "logfile /var/log/redis/redis.log" >> /etc/redis/redis.conf
echo " " >> /etc/redis/redis.conf
echo "port 6379" >> /etc/redis/redis.conf
echo "bind 127.0.0.1" >> /etc/redis/redis.conf
echo "timeout 300" >> /etc/redis/redis.conf
echo " " >> /etc/redis/redis.conf
echo "loglevel notice" >> /etc/redis/redis.conf
echo " " >> /etc/redis/redis.conf
echo "## Default configuration options" >> /etc/redis/redis.conf
echo "databases 16" >> /etc/redis/redis.conf
echo " " >> /etc/redis/redis.conf
echo "save 900 1" >> /etc/redis/redis.conf
echo "save 300 10" >> /etc/redis/redis.conf
echo "save 60 10000" >> /etc/redis/redis.conf
echo " " >> /etc/redis/redis.conf
echo "rdbcompression yes" >> /etc/redis/redis.conf
echo "dbfilename dump.rdb" >> /etc/redis/redis.conf
echo " " >> /etc/redis/redis.conf
echo "appendonly no" >> /etc/redis/redis.conf
echo " " >> /etc/redis/redis.conf

service redis-server restart

echo "Redis install tutorial from:" >> $DIR/readme.txt
echo "https://www.linode.com/docs/databases/redis/redis-on-ubuntu-12-04-precise-pangolin" >> $DIR/readme.txt
echo " " >> $DIR/readme.txt
echo " --//-- " >> $DIR/readme.txt
