#!/bin/bash

DIR=`pwd`

echo " " >> $DIR/readme.txt
echo " - mail.sh - " >> $DIR/readme.txt

service sendmail stop; update-rc.d -f sendmail remove
apt-get install -y postfix postfix-mysql postfix-doc openssl getmail4 mailutils

echo "installed postfix postfix-mysql postfix-doc openssl getmail4 mailutils " >> $DIR/readme.txt

echo " --//-- " >> $DIR/readme.txt
