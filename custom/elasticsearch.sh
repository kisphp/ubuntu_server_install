#!/bin/bash

apt-get install -y openjdk-6-jre

add-apt-repository ppa:webupd8team/java

apt-get install -y oracle-java7-installer

wget https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-0.90.9.deb

ifconfig | grep 'inet addr'

echo " "
echo "Type server public IP"
echo " "

read NEW_IP

dpkg -i elasticsearch-0.90.9.deb

rm elasticsearch-0.90.9.deb

echo "network.bind_host: $NEW_IP" >> /etc/elasticsearch/elasticsearch.yml
echo "script.disable_dynamic: true" >> /etc/elasticsearch/elasticsearch.yml

/usr/share/elasticsearch/bin/plugin -install mobz/elasticsearch-head
/usr/share/elasticsearch/bin/plugin -install elasticsearch/marvel/latest

service elasticsearch restart

