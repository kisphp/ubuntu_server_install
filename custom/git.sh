#!/bin/bash

DIR=`pwd`

echo " " >> $DIR/readme.txt
echo " - custom/git.sh - " >> $DIR/readme.txt

apt-get install -y git
echo "installed git" >> $DIR/readme.txt

git config --global user.email "mariusbogdan83@gmail.com"
git config --global user.name "Marius Bogdan"

echo " --//-- " >> $DIR/readme.txt

