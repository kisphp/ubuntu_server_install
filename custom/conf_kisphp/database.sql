CREATE DATABASE `kisphp_sdk` COLLATE 'utf8_general_ci';
CREATE USER 'kisphp'@'localhost' IDENTIFIED BY 'kisphp';
GRANT USAGE ON *.* TO 'kisphp'@'localhost';
GRANT SELECT, EXECUTE, SHOW VIEW, ALTER, ALTER ROUTINE, CREATE, CREATE ROUTINE, CREATE TEMPORARY TABLES, CREATE VIEW, DELETE, DROP, EVENT, INDEX, INSERT, REFERENCES, TRIGGER, UPDATE, LOCK TABLES  ON `kisphp_%`.* TO 'kisphp'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;

USE `kisphp_sdk`;

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `articles`;
CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_category` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT '',
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

INSERT INTO `articles` (`id`, `id_category`, `title`, `content`) VALUES
  (1, 3, 'updated article', 'new article content updated'),
  (2, 2, 'modified article added from OOP 1419945812', 'description book 2'),
  (3, 2, 'Video 1', 'description video 1'),
  (4, 2, 'Video 2', 'description video 2'),
  (5, 2, 'modified article added from OOP', 'new article content'),
  (6, 2, 'modified article added from OOP', 'new article content'),
  (7, 2, 'modified article added from OOP', 'new article content'),
  (8, 2, 'modified article added from OOP', 'new article content'),
  (9, 2, 'modified article added from OOP', 'new article content'),
  (10, 2, 'modified article added from OOP', 'new article content'),
  (11, 2, 'new name for my article', NULL),
  (12, 2, 'new name for my article', NULL),
  (13, 2, 'new name for my article', NULL),
  (14, 2, 'new name for my article', NULL),
  (15, 2, 'new name for my article', NULL),
  (16, 2, 'my new article added from OOP', NULL),
  (17, 2, 'my new article added from OOP', NULL),
  (18, 2, 'my new article added from OOP', NULL);

DROP TABLE IF EXISTS `articles_categories`;
CREATE TABLE IF NOT EXISTS `articles_categories` (
  `id_articles` int(10) unsigned NOT NULL DEFAULT '0',
  `id_categories` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_articles`,`id_categories`),
  KEY `FK_articles_categories_categories` (`id_categories`),
  CONSTRAINT `FK_articles_categories_categories` FOREIGN KEY (`id_categories`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK__articles` FOREIGN KEY (`id_articles`) REFERENCES `articles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `articles_categories` (`id_articles`, `id_categories`) VALUES
  (5, 1);

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `categories` (`id`, `title`) VALUES
  (1, 'Book'),
  (2, 'Comicbook'),
  (3, 'articles');

SET FOREIGN_KEY_CHECKS = 1;