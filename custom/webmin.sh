#!/bin/bash

DIR=`pwd`

echo " " >> $DIR/readme.txt
echo " - webmin.sh - " >> $DIR/readme.txt

apt-get install -f -y
echo "make sure there are no packages broken on installation or not pending " >> $DIR/readme.txt

apt-get install -y libnet-ssleay-perl libauthen-pam-perl libio-pty-perl apt-show-versions libapt-pkg-perl
echo "install libnet-ssleay-perl libauthen-pam-perl libio-pty-perl apt-show-versions libapt-pkg-perl " >> $DIR/readme.txt

wget http://prdownloads.sourceforge.net/webadmin/webmin_1.730_all.deb && dpkg --install webmin_1.730_all.deb
echo "download webmin and install it " >> $DIR/readme.txt

rm webmin_1.730_all.deb
echo "remove webmin installtion file " >> $DIR/readme.txt

echo "view webmin at " >> $DIR/readme.txt
echo "http://deb.local:10000/ " >> $DIR/readme.txt
echo "login with server root user " >> $DIR/readme.txt

# view webmin at
# http://deb.local:10000/
# login with server root user

echo " --//-- " >> $DIR/readme.txt
