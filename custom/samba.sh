#!/bin/bash

DIR=`pwd`

echo " " >> $DIR/readme.txt
echo " - samba.sh - " >> $DIR/readme.txt

apt-get install -y samba samba-common python-glade2 system-config-samba
echo "installed samba samba-common python-glade2 system-config-samba - " >> $DIR/readme.txt

addgroup smbgrp
echo "added group smbgrp - " >> $DIR/readme.txt

usermod -a -G smbgrp "$SUSER"
echo "added $SUSER to group smbgrp - " >> $DIR/readme.txt

smbpasswd -a "$SUSER"
echo "created samba user $SUSER - " >> $DIR/readme.txt

echo "$SUSER = \"$SUSER\"" > /etc/samba/smbusers
echo "created /etc/samba/smbusers - " >> $DIR/readme.txt

/etc/init.d/samba restart
echo "restart samba - " >> $DIR/readme.txt

cp /etc/samba/smb.conf /etc/samba/smb.conf.bkp
cp $DIR/conf/smb.conf /etc/samba/smb.conf
echo "updated samba config - " >> $DIR/readme.txt

echo " --//-- " >> $DIR/readme.txt
