#!/bin/bash

DIR=`pwd`

echo " " >> $DIR/readme.txt
echo " - mysql.sh - " >> $DIR/readme.txt

MYSQL_PASSWORD='debian'

debconf-set-selections <<< "mysql-server mysql-server/root_password password $MYSQL_PASSWORD"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $MYSQL_PASSWORD"

echo "Set mysql password to '$MYSQL_PASSWORD' for user 'root'" >> $DIR/readme.txt

apt-get install -y mysql-client mysql-server

echo "installed mysql-client mysql-server" >> $DIR/readme.txt

MYSQL=`which mysql`

echo " -------------------------------------------------------------------------------- "
echo " - Add database password for user root ------------------------------------------ "
echo " - Create user \"root\" with password \"debian\" to have access from remote servers - "
echo " -------------------------------------------------------------------------------- "
$MYSQL -uroot -p -e "CREATE USER 'root'@'%' IDENTIFIED BY '$MYSQL_PASSWORD'; GRANT ALL PRIVILEGES ON * . * TO 'root'@'%';"
echo "created user root with password $MYSQL_PASSWORD for mysql and allow connect from any host " >> $DIR/readme.txt

/etc/init.d/mysql restart

echo " --//-- " >> $DIR/readme.txt
