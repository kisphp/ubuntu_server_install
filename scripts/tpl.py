#!/usr/bin/python

import getpass, sys, subprocess

# get current username
user = ''

# get filename passed as parameter
filename = sys.argv[1]

# read system user from the passwd file
with open('/etc/passwd') as f:
    # file_str = f.read()
    for line in f.read().split("\n"):
        if '1000' in line:
            pos = line.find(':');
            user = line[0:pos]
            break

if user == '':
    user = 'root'

# open file passed as parameter
with open(filename) as f:
    file_str = f.read()

# change username in file
file_str = file_str.replace('{{SUSER}}', user)

# write content to file
with open(filename, 'w') as f:
   f.write(file_str)
