#!/bin/bash

DIR=`pwd`

echo " " >> $DIR/readme.txt
echo " - fix-locales.sh " >> $DIR/readme.txt

echo " regenerate locales " >> $DIR/readme.txt

locale-gen en_US.UTF-8

localedef -i en_US -f UTF-8 en_US.UTF-8

export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
locale-gen en_US.UTF-8

dpkg-reconfigure locales

echo " dpkg-reconfigure locales " >> $DIR/readme.txt
echo " --//-- " >> $DIR/readme.txt

