#!/bin/bash

echo " " >> $DIR/readme.txt
echo "- apache_php.sh - " >> $DIR/readme.txt

apt-get install -y apache2 apache2-doc apache2-utils libapache2-mod-php5 php5 php5-common php5-gd php5-mysql php5-imap php5-cli php5-cgi libapache2-mod-fcgid apache2-suexec php-pear php-auth php5-mcrypt mcrypt php5-imagick imagemagick libapache2-mod-suphp libruby libapache2-mod-python php5-curl php5-intl php5-memcache php5-memcached php5-ming php5-ps php5-pspell php5-recode php5-snmp php5-sqlite php5-tidy php5-xmlrpc php5-xsl memcached snmp

echo "installed apache2 apache2-doc apache2-utils libapache2-mod-php5 php5 php5-common php5-gd php5-mysql php5-imap php5-cli php5-cgi libapache2-mod-fcgid apache2-suexec php-pear php-auth php5-mcrypt mcrypt php5-imagick imagemagick libapache2-mod-suphp libruby libapache2-mod-python php5-curl php5-intl php5-memcache php5-memcached php5-ming php5-ps php5-pspell php5-recode php5-snmp php5-sqlite php5-tidy php5-xmlrpc php5-xsl memcached snmp " >> $DIR/readme.txt

php5enmod mcrypt
echo "enabled php -> mcrypt " >> $DIR/readme.txt

a2enmod rewrite ssl actions include
echo "enabled apache2 -> rewrite ssl actions include " >> $DIR/readme.txt
a2enmod dav_fs dav auth_digest
echo "enabled apache2 -> dav_fs dav auth_digest " >> $DIR/readme.txt
apt-get install -y php5-xcache
echo "installed php5-xcache " >> $DIR/readme.txt
#apt-get install -y libapache2-mod-fastcgi php5-fpm
apt-get install -y php5-fpm
echo "installed php5-fpm " >> $DIR/readme.txt

a2enmod actions fastcgi alias
echo "enabled apache2 -> actions fastcgi alias " >> $DIR/readme.txt

echo " --//-- " >> $DIR/readme.txt
