#!/bin/bash

DIR=`pwd`

echo " " >> $DIR/readme.txt
echo " - utils.sh - " >> $DIR/readme.txt

apt-get update
apt-get upgrade -y

apt-get install -y mc tree vim
echo " installed mc tree vim " >> $DIR/readme.txt

apt-get install -y ssh openssh-server
echo " installed ssh openssh-server " >> $DIR/readme.txt
apt-get install -y binutils sudo
echo " installed binutils sudo " >> $DIR/readme.txt

apt-get install -y gcc make dos2unix
echo " installed gcc make dos2unix " >> $DIR/readme.txt
echo " --//-- " >> $DIR/readme.txt

