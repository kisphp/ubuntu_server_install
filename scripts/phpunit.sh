#!/bin/bash

DIR=`pwd`

echo " " >> $DIR/readme.txt
echo " - phpunit.sh - " >> $DIR/readme.txt

wget https://phar.phpunit.de/phpunit.phar

chmod +x phpunit.phar

mv phpunit.phar /usr/local/bin/phpunit

echo "installed phpunit" >> $DIR/readme.txt
echo " --//-- " >> $DIR/readme.txt
