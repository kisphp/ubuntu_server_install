#!/bin/bash

DIR=`pwd`

echo " " >> $DIR/readme.txt
echo " - remove_apparmor.sh - " >> $DIR/readme.txt

service apparmor stop
update-rc.d -f apparmor remove
apt-get remove -y apparmor apparmor-utils

echo " --//-- " >> $DIR/readme.txt
