#!/bin/bash

DIR=`pwd`

echo " " >> $DIR/readme.txt
echo " - composer.sh - " >> $DIR/readme.txt

curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

echo "composer installed" >> $DIR/readme.txt
echo " --//-- " >> $DIR/readme.txt
