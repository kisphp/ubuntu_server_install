#!/bin/bash

DIR=`pwd`

echo "---------" > $DIR/readme.txt
echo " READ ME " >> $DIR/readme.txt
echo "---------" >> $DIR/readme.txt
echo " " >> $DIR/readme.txt

dpkg-reconfigure dash
echo "reconfigure dash " >> $DIR/readme.txt

. $DIR/scripts/fix-locales.sh
. $DIR/scripts/utils.sh
. $DIR/scripts/remove_apparmor.sh
. $DIR/scripts/ntp.sh
. $DIR/scripts/mysql.sh
. $DIR/scripts/apache_php.sh
. $DIR/scripts/composer.sh
. $DIR/scripts/phpunit.sh

SUSER=`cat /etc/passwd | grep 1000 | cut -d: -f1`

echo "created file /etc/update-motd.d/95-info" >> $DIR/readme.txt
cp $DIR/conf/95-info /etc/update-motd.d/95-info
echo "given executable permissions to motd file" >> $DIR/readme.txt
chmod u+x /etc/update-motd.d/95-info

echo "update vimrc file" >> $DIR/readme.txt
cp /etc/vim/vimrc /etc/vim/vimrc.bkp
cp $DIR/conf/vimrc /etc/vim/vimrc
echo "update postfix master config file" >> $DIR/readme.txt
cp $DIR/conf/master.cf /etc/postfix/master.cf
echo "update mysql config file" >> $DIR/readme.txt
cp $DIR/conf/my.cnf /etc/mysql/my.cnf

echo "backup apache suphp and fastcgi config files" >> $DIR/readme.txt
cp /etc/apache2/mods-available/suphp.conf /etc/apache2/mods-available/suphp.conf.bkp
cp /etc/apache2/mods-available/fastcgi.conf /etc/apache2/mods-available/fastcgi.conf.bkp

echo "update apache suphp and fastcgi config files" >> $DIR/readme.txt
cp $DIR/conf/suphp.conf /etc/apache2/mods-available/suphp.conf
cp $DIR/conf/fastcgi.conf /etc/apache2/mods-available/fastcgi.conf

echo "copy .dircolors file to root" >> $DIR/readme.txt
cp $DIR/conf/.dircolors  ~/
echo "copy .dircolors file to user" >> $DIR/readme.txt
cp $DIR/conf/.dircolors /home/$SUSER/
chown $SUSER: /home/$SUSER/.dircolors

echo "parse and change apache2.conf content with python" >> $DIR/readme.txt
PY=`which python`
$PY $DIR/scripts/tpl.py $DIR/conf/apache2.conf

echo "create /home/web directory and give rights to user" >> $DIR/readme.txt
mkdir /home/web -p
chown $SUSER: /home/web -R

echo "update apache config file" >> $DIR/readme.txt
cp /etc/apache2/apache2.conf /etc/apache2/apache2.conf.bkp
cp $DIR/conf/apache2.conf /etc/apache2/apache2.conf

echo "backup and update php config files" >> $DIR/readme.txt
cp /etc/php5/apache2/php.ini /etc/php5/apache2/php.ini.bkp
cp $DIR/conf/php.ini /etc/php5/apache2/php.ini

echo "alias apare='/etc/init.d/apache2 restart'" >> ~/.bashrc
echo "alias myre='/etc/init.d/mysql restart'" >> ~/.bashrc
echo "alias apare='sudo /etc/init.d/apache2 restart'" >> /home/$SUSER/.bashrc
echo "alias myre='sudo /etc/init.d/mysql restart'" >> /home/$SUSER/.bashrc
echo "alias halt='sudo halt -p && exit'" >> /home/$SUSER/.bashrc

source /root/.bashrc

chown $SUSER: /home/web -R

service apache2 restart
service mysql restart

for file in `ls ./custom/`
do
    if [[ $file == *".sh" ]]
    then
        #echo $file
        echo " "
        echo "Would you like to install ${file/\.sh/} ?"
        echo " [y|n]"
        read answer
        if [[ $answer == 'y' || $answer == 'Y' ]]
        then
            . custom/$file
        fi
    fi
done

cat $DIR/readme.txt
